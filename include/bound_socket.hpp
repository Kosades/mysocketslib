#ifndef BOUND_SOCKET_H
#define BOUND_SOCKET_H
#include "socket.hpp"

namespace Sockets {

/*socket that is always binded to existing file descriptor*/
class BoundSocket : public Socket {
private:
  int boundFD;
  void bind();

protected:
  BoundSocket(int FD, std::string address, uint16_t port);

public:
  BoundSocket(std::string address, uint16_t port);
  /*Socket is automaticaly closed on destructor*/
  ~BoundSocket();

  const int getFileDescriptor();
};

}
#endif
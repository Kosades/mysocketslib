#ifndef CONNECT_SOCKET_H
#define CONNECT_SOCKET_H
#include "bound_socket.hpp"

namespace Sockets {

class ConnectSocket : public BoundSocket {
private:
  bool isConnected;

protected:
  Socket connectedTo;

public:
  /* Will create socket with given address and random port(if not specified) */
  ConnectSocket(std::string address, uint16_t port = 0);

  ConnectSocket(int FD, std::string currentAddress, uint16_t currentPort,
                std::string connectedAddress, uint16_t connectedPort);

  /*Connects this socket with given address and port
   * if it have been already connected then it dosconnects from current
   * connection and creates new connected socket with same port and
   * address(except FD)
   */
  void connect(std::string address, int port);
  /*return pointer to receive data buffer with set length*/
  char *receive(size_t receiveBytes);
  /*sends buffer with length bufferLength + 1*/
  void send(char *buffer, size_t bufferLength);

  // Getters
  const Socket &getConnectedSocket();
  bool isSocketConnected();
};

} // namespace Sockets
#endif
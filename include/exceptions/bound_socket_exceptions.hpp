#ifndef BOUND_SOCKET_EXCEPTIONS_HPP
#define BOUND_SOCKET_EXCEPTIONS_HPP

#include "socket_exceptions.hpp"
#include <error.h>
#include <iostream>

namespace Sockets {

struct AccessDeniedException : BindException,
                               SocketCreationException,
                               ConnectException,
                               ReceiveException,
                               SendException {
  const char *what() const throw();
};

struct AddressInUseException : BindException,
                               ConnectException,
                               ListenException {
  const char *what() const throw();
};

struct InvalidAddressException : BindException,
                                 ConnectException,
                                 SendException {
  const char *what() const throw();
};
struct AddressNotAvailableException : BindException,
                                      ConnectException,
                                      AcceptException {
  const char *what() const throw();
};

struct AddressToLongException : BindException {
  const char *what() const throw();
};

struct LackKernelMemoryException : BindException {
  const char *what() const throw();
};

struct FileLimitException : SocketCreationException, AcceptException {
  const char *what() const throw();
};
struct SystemFileLimitException : FileLimitException {
  const char *what() const throw();
};
struct InsufficientMemoryException : SocketCreationException,
                                     BindException,
                                     ReceiveException,
                                     SendException,
                                     AcceptException {
  const char *what() const throw();
};

BindException *parseBindErrorCode(int errorCode);
SocketCreationException *parseSocketErrorCode(int errorCode);

} // namespace Sockets
#endif
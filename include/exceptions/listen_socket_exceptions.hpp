#ifndef LISTEN_SOCKET_EXCEPTIONS_HPP
#define LISTEN_SOCKET_EXCEPTIONS_HPP

#include "connected_sockets_exceptions.hpp"

namespace Sockets {

struct NotListeningException : AcceptException {
  const char *what() const throw();
};

struct ProtocolException : AcceptException {
  const char *what() const throw();
};

struct ForbiddenByFirewallExcpetion : AcceptException {
  const char *what() const throw();
};

AcceptException *parseAcceptErrorCode(int errorCode);
ListenException *parseListenErrorCode(int errorCode);

} // namespace Sockets
#endif
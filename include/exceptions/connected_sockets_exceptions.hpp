#ifndef CONNECTED_SOCKET_EXCEPTIONS_HPP
#define CONNECTED_SOCKET_EXCEPTIONS_HPP

#include "bound_socket_exceptions.hpp"
#include <exception>

namespace Sockets {

struct NoDataAvailableException : ConnectException,
                                  ReceiveException,
                                  SendException,
                                  AcceptException {
  const char *what() const throw();
};
struct ConnectionRefusedException : ConnectException,
                                    ReceiveException,
                                    AcceptException {
  const char *what() const throw();
};
struct NonCompletedConnectionException : ConnectException {
  const char *what() const throw();
};
struct NoImmediateConnectionException : ConnectException {
  const char *what() const throw();
};
struct AlreadyConnectedException : ConnectException {
  const char *what() const throw();
};
struct UnreachableNetworkException : ConnectException {
  const char *what() const throw();
};
struct TimeoutException : ConnectException {
  const char *what() const throw();
};
struct NotConnectedException : ReceiveException, SendException {
  const char *what() const throw();
};

struct AlreadyInProgress : SendException {
  const char *what() const throw();
};
struct ConnectionResetException : SendException {
  const char *what() const throw();
};

struct OutputQueueFullException : SendException {
  const char *what() const throw();
};

ConnectException *parseConnectErrorCode(int errorCode);
ReceiveException *parseReceiveErrorCode(int errorCode);
SendException *parseSendErrorCode(int errorCode);

} // namespace Sockets
#endif
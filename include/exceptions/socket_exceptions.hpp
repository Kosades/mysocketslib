#ifndef SOCKET_EXCEPTIONS_HPP
#define SOCKET_EXCEPTIONS_HPP

#include <exception>

namespace Sockets {

// Base socket
struct SocketException : std::exception {
  const char *what() const throw();
};
struct AddressConvertException : SocketException {
  const char *what() const throw();
};

struct BinaryConvertException : SocketException {
  const char *what() const throw();
};

// Bound socket
struct BindException : SocketException {
  const char *what() const throw();
};
struct SocketCreationException : SocketException {
  const char *what() const throw();
};

// Connect socket
struct ConnectException : SocketException {
  const char *what() const throw();
};
struct ReceiveException : SocketException {
  const char *what() const throw();
};
struct SendException : SocketException {
  const char *what() const throw();
};
// Listen socket
struct AcceptException : SocketException {
  const char *what() const throw();
};
struct ListenException : SocketException {
  const char *what() const throw();
};

} // namespace Sockets
#endif
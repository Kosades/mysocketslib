#ifndef SOCKETS_LIB
#define SOCKETS_LIB

#include <arpa/inet.h>
#include <cstring>
#include <exception>
#include <functional>
#include <list>
#include <thread>

namespace Sockets {
/*Class that contains simple data about socket*/
class Socket {
protected:
  /*paramaters got from base C library*/
  sockaddr_in socketHint;

public:
  Socket(std::string address, uint16_t port);

  // Getters
  uint16_t getPort() const;
  std::string getAddress() const;
  const sockaddr_in &getSocketHint() const;
};
/* Class that always contains socket that is already bound with file
 * descriptor*/
class BoundSocket : public Socket {
private:
  int boundFD;
  void bind();

protected:
  BoundSocket(int FD, std::string address, uint16_t port);

public:
  BoundSocket(std::string address, uint16_t port);
  /*Socket is automaticaly closed on destructor*/
  ~BoundSocket();
};

/* class that represents socket that can connect to some other addresses and can
 * send and receive some data*/
class ConnectSocket : public BoundSocket {
private:
  bool isConnected;

protected:
  Socket
      connectedTo; /* connected socket(at construction has duplicate values)*/
public:
  /* If port is not set, then creates socket with any available port*/
  ConnectSocket(std::string address, uint16_t port = 0);

  /*Connects this socket with given address and port
   * if it have been already connected then it dosconnects from current
   * connection and creates new connected socket with same port and
   * address(except FD)
   */
  void connect(std::string address, int port);
  /*return pointer to received data buffer with set length*/
  char *receive(size_t receiveBytes);
  /*sends buffer with length bufferLength + 1*/
  void send(char *buffer, size_t bufferLength);

  // Getters
  const Socket &getConnectedSocket();
  bool isSocketConnected();
};

/* class that represents socket that can be put in listen mod and can create new
 * connected sockets*/
class ListenSocket : public BoundSocket {
private:
  bool isListening;
  // Threads
  std::thread listenThread;
  std::list<std::thread> threads;

  /*function that will be put in the listenThread*/
  void listener(std::function<void(ConnectSocket)> onConnected);

  ConnectSocket accept();

public:
  ListenSocket(std::string listenAddress, uint16_t listenPort);
  /*Marks socket for listening and creates listen thread.
   * When other socket is connected it starts thread with onConnected function
   * and after its completion deletes it. Parameter function works with created
   * on connection socket*/
  void listen(std::function<void(ConnectSocket)> onConnected);
  /* closes current FD and creates new one with same properties(except FD) */
  void stopListen();
  bool isSocketListening();
  ~ListenSocket(); /*on destructor joins all running threads*/
};

// Exceptions
// Base
struct SocketException : std::exception {
  const char *what() const throw();
};
struct AddressConvertException : SocketException {
  const char *what() const throw();
};

struct BinaryConvertException : SocketException {
  const char *what() const throw();
};

// Bound socket
struct BindException : SocketException {
  const char *what() const throw();
};
struct SocketCreationException : SocketException {
  const char *what() const throw();
};

// Connect socket
struct ConnectException : SocketException {
  const char *what() const throw();
};
struct ReceiveException : SocketException {
  const char *what() const throw();
};
struct SendException : SocketException {
  const char *what() const throw();
};
// Listen socket
struct AcceptException : SocketException {
  const char *what() const throw();
};
struct ListenException : SocketException {
  const char *what() const throw();
};

// More complicated
struct AccessDeniedException : BindException, SocketCreationException {
  const char *what() const throw();
};
struct AddressInUseException : BindException {
  const char *what() const throw();
};
struct InvalidAddressException : BindException {
  const char *what() const throw();
};
struct AddressNotAvailableException : BindException {
  const char *what() const throw();
};
struct UnaccessibleAddressException : BindException {
  const char *what() const throw();
};
struct AddressToLongException : BindException {
  const char *what() const throw();
};
struct LackKernelMemoryException : BindException {
  const char *what() const throw();
};
struct FileLimitException : SocketCreationException {
  const char *what() const throw();
};
struct SystemFileLimitException : FileLimitException {
  const char *what() const throw();
};
struct InsufficientMemoryException : SocketCreationException,
                                     BindException,
                                     ReceiveException {
  const char *what() const throw();
};
struct NoDataAvailableException : ConnectException {
  const char *what() const throw();
};
struct ConnectionRefusedException : ConnectException {
  const char *what() const throw();
};
struct NonCompletedConnectionException : ConnectException {
  const char *what() const throw();
};
struct NoImmediateConnectionException : ConnectException {
  const char *what() const throw();
};
struct AlreadyConnectedException : ConnectException {
  const char *what() const throw();
};
struct UnreachableNetworkException : ConnectException {
  const char *what() const throw();
};
struct TimeoutException : ConnectException {
  const char *what() const throw();
};

// Exception parsers
BindException *parseBindErrorCode(int errorCode);
SocketCreationException *parseSocketErrorCode(int errorCode);
AcceptException *parseAcceptErrorCode(int errorCode);
ListenException *parseListenErrorCode(int errorCode);
ConnectException *parseConnectErrorCode(int errorCode);
ReceiveException *parseReceiveErrorCode(int errorCode);
SendException *parseSendErrorCode(int errorCode);

} // namespace Sockets
#endif
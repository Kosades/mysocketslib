#ifndef LISTEN_SOCKET_H
#define LISTEN_SOCKET_H

#include "bound_socket.hpp"
#include "connect_socket.hpp"
#include <functional>
#include <list>
#include <thread>

namespace Sockets {

class ListenSocket : public BoundSocket {
private:
  bool isListening;
  // Threads
  std::thread listenThread;
  std::list<std::thread> threads;

  /*function that will be put in the listenThread*/
  void listener(std::function<void(ConnectSocket)> onConnected);

  ConnectSocket accept();

protected:
public:
  ListenSocket(std::string listenAddress, uint16_t listenPort);
  /*Marks socket for listening and creates listen thread.
   * When other socket is connected it starts thread with onConnected function
   * and after its completion deletes it. Parameter function works with created
   * on connection socket*/
  void listen(std::function<void(ConnectSocket)> onConnected);
  bool isSocketListening();
  ~ListenSocket();
};

} // namespace Sockets
#endif
#include "../../include/bound_socket.hpp"
#include "../../include/connect_socket.hpp"
#include "../../include/exceptions/bound_socket_exceptions.hpp"
#include "../../include/listen_socket.hpp"
#include <iostream>
#include <string>

using namespace Sockets;

void handler(ConnectSocket socket) {
  socket.send("Test message", 12);
  std::cout << "Recieved: \n" << socket.recieve(sizeof(char));
}

int main() {
  // try {
  ListenSocket socket("0.0.0.0", 54000);
  socket.listen(handler);

  /*} catch (BindException *exc) {
    std::cerr << exc->what();
  }*/
}
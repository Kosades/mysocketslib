#include <sockets.hpp>
#include <iostream>
#include <string>

using namespace Sockets;

void handler(ConnectSocket socket) {
  socket.send("Test message", 12);
  std::cout << "Recieved: \n" << socket.recieve(sizeof(char));
}

int main() {
  try {
    ListenSocket socket("0.0.0.0", 54000);
    socket.listen(handler);

  } catch (BindException *exc) {
    std::cerr << exc->what();
  }
}
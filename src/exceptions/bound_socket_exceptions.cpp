#include "exceptions/bound_socket_exceptions.hpp"

using namespace Sockets;

const char *AccessDeniedException::what() const throw() {
  return "You don't have right access to interact with this socket";
}

const char *AddressInUseException::what() const throw() {
  return "Given address or port is already used.";
}

const char *InvalidAddressException::what() const throw() {
  return "Given address or port are invalid.";
}

const char *AddressNotAvailableException::what() const throw() {
  return "Requested address is not local.";
}

const char *AddressToLongException::what() const throw() {
  return "Address or port are too long";
}

const char *LackKernelMemoryException::what() const throw() {
  return "Insufficient kernel memory was available.";
}

const char *FileLimitException::what() const throw() {
  return "Limit of opened per process file descriptors has reached.";
}

const char *SystemFileLimitException::what() const throw() {
  return "System limit of opened per process file descriptors has reached.";
}

const char *InsufficientMemoryException::what() const throw() {
  return "Insufficient memory available to perform this operation.";
}

BindException *Sockets::parseBindErrorCode(int errorCode) {
  switch (errorCode) {
  case EACCES: {
    return new AccessDeniedException();
  }
  case EADDRINUSE: {
    return new AddressInUseException();
  }
  case EINVAL: {
    return new InvalidAddressException();
  }
  case EADDRNOTAVAIL: {
    return new AddressNotAvailableException();
  }
  case EFAULT: {
    return new AddressNotAvailableException();
  }
  case ENAMETOOLONG: {
    return new AddressToLongException();
  }
  case ENOMEM: {
    return new LackKernelMemoryException();
  }
  default: {
    return new BindException();
  }
  }
}
SocketCreationException *Sockets::parseSocketErrorCode(int errorCode) {
  switch (errorCode) {
  case EACCES: {
    return new AccessDeniedException();
  }
  case EMFILE: {
    return new FileLimitException();
  }
  case ENFILE: {
    return new SystemFileLimitException();
  }
  case ENOBUFS: {
    return new InsufficientMemoryException();
  }
  case ENOMEM: {
    return new InsufficientMemoryException();
  }
  default:
    return new SocketCreationException();
  }
}

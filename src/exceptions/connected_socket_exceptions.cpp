#include "exceptions/connected_sockets_exceptions.hpp"

using namespace Sockets;

const char *NoDataAvailableException::what() const throw() {
  return "No data available in routing cache.";
}

const char *ConnectionRefusedException::what() const throw() {
  return "Connection refused or address is not existing.";
}

const char *NonCompletedConnectionException::what() const throw() {
  return "The socket is nonblocking and a previous connection attempt has "
         "not yet been completed.";
}

const char *NoImmediateConnectionException::what() const throw() {
  return "Connection cannot be established immediately.";
}

const char *AlreadyConnectedException::what() const throw() {
  return "The socket is already connected.";
}

const char *UnreachableNetworkException::what() const throw() {
  return "The network is unreachable.";
}

const char *TimeoutException::what() const throw() {
  return "Connection timed out";
}

const char *NotConnectedException::what() const throw() {
  return "The socket is not connected.";
}

const char *AlreadyInProgress::what() const throw() {
  return "This proccess is already in progress.";
}

const char *ConnectionResetException::what() const throw() {
  return "Connection was reset by peer.";
}

const char *OutputQueueFullException::what() const throw() {
  return "Output queue for network interface is full.";
}

ConnectException *Sockets::parseConnectErrorCode(int errorCode) {
  switch (errorCode) {
  case EADDRINUSE: {
    return new AddressInUseException();
  }
  case EACCES: {
    return new AccessDeniedException();
  }
  case EAFNOSUPPORT: {
    return new InvalidAddressException();
  }
  case EAGAIN: {
    return new NoDataAvailableException();
  }
  case ECONNREFUSED: {
    return new ConnectionRefusedException();
  }
  case EALREADY: {
    return new NonCompletedConnectionException();
  }
  case EFAULT: {
    return new AddressNotAvailableException();
  }
  case EINPROGRESS: {
    return new NoImmediateConnectionException();
  }
  case EISCONN: {
    return new AlreadyConnectedException();
  }
  case ENETUNREACH: {
    return new UnreachableNetworkException();
  }
  case ETIMEDOUT: {
    return new TimeoutException();
  }
  default: {
    return new ConnectException();
  }
  }
}

ReceiveException *Sockets::parseReceiveErrorCode(int errorCode) {
  switch (errorCode) {
  case EAGAIN: {
    return new NoDataAvailableException();
  }
  case ECONNREFUSED: {
    return new ConnectionRefusedException();
  }
  case ENOTCONN: {
    return new NotConnectedException();
  }
  default: {
    return new ReceiveException();
  }
  }
}

SendException *Sockets::parseSendErrorCode(int errorCode) {
  switch (errorCode) {
  case EACCES: {
    return new AccessDeniedException();
  }
  case EAGAIN: {
    return new NoDataAvailableException();
  }
  case EALREADY: {
    return new AlreadyInProgress();
  }
  case ECONNRESET: {
    return new ConnectionResetException();
  }
  case EDESTADDRREQ: {
    return new NotConnectedException();
  }
  case EFAULT: {
    return new InvalidAddressException();
  }
  case ENOBUFS: {
    return new OutputQueueFullException();
  }
  case ENOMEM: {
    return new InsufficientMemoryException();
  }
  case ENOTCONN: {
    return new NotConnectedException();
  }
  default: {
    return new SendException();
  }
  }
}

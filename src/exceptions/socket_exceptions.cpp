#include "exceptions/socket_exceptions.hpp"

using namespace Sockets;

const char *SocketException::what() const throw() {
  return "Some error acquired in socket class";
};

const char *AddressConvertException::what() const throw() {
  return "Failed to convert string address to binary format\nWrong IP address "
         "format";
};

const char *BinaryConvertException::what() const throw() {
  return "Failed to convert binary value to string address\nThe converted "
         "address string exceeded the size";
};

// Bound socket

const char *BindException::what() const throw() {
  return "There was bind exception";
}

const char *SocketCreationException::what() const throw() {
  return "There was an error while creating socket";
};

// Connect socket
const char *ConnectException::what() const throw() {
  return "Exception while connecting.";
};

const char *ReceiveException::what() const throw() {
  return "Exception while receiving data";
}

const char *SendException::what() const throw() {
  return "Exception while sending data.";
}

// Listen socket
const char *AcceptException::what() const throw() {
  return "Error occurred while accepting connection.";
};

const char *ListenException::what() const throw() {
  return "Error ocurred while putting socket o listening.";
}

#include "exceptions/listen_socket_exceptions.hpp"

using namespace Sockets;

const char *NotListeningException::what() const throw() {
  return "Current socket is not set to listen mode.";
}

const char *ProtocolException::what() const throw() {
  return "Protocol error.";
}

const char *ForbiddenByFirewallExcpetion::what() const throw() {
  return "Firewall rules forbid connection.";
}

AcceptException *Sockets::parseAcceptErrorCode(int errorCode) {
  switch (errorCode) {
  case EAGAIN: {
    return new NoDataAvailableException();
  }
  case ECONNABORTED: {
    return new ConnectionRefusedException();
  }
  case EFAULT: {
    return new AddressNotAvailableException();
  }
  case EINVAL: {
    return new NotListeningException();
  }
  case EMFILE: {
    return new FileLimitException();
  }
  case ENFILE: {
    return new SystemFileLimitException();
  }
  case ENOBUFS: {
    return new InsufficientMemoryException();
  }
  case EPROTO: {
    return new ProtocolException();
  }
  case EPERM: {
    return new ForbiddenByFirewallExcpetion();
  }
  default: {
    return new AcceptException();
  }
  }
}

ListenException *Sockets::parseListenErrorCode(int errorCode) {
  switch (errorCode) {
  case EADDRINUSE: {
    return new AddressInUseException();
  }
  default: {
    return new ListenException();
  }
  }
}

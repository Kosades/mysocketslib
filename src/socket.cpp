#include "socket.hpp"
#include "exceptions/socket_exceptions.hpp"
#include <cstring>
using namespace Sockets;

Socket::Socket(std::string address, uint16_t port) {
  memset(&socketHint, 0, sizeof(socketHint));

  socketHint.sin_family = AF_INET;
  // Setting port and address
  socketHint.sin_port = htons(port);

  if (inet_pton(AF_INET, address.data(), &socketHint.sin_addr) == 0) {
    throw AddressConvertException();
  }
}

uint16_t Socket::getPort() const { return ntohs(socketHint.sin_port); }
std::string Socket::getAddress() const {
  char address[INET_ADDRSTRLEN];
  if (inet_ntop(AF_INET, &socketHint.sin_addr, address, INET_ADDRSTRLEN) ==
      NULL) {
    throw BinaryConvertException();
  }
  return address;
}
const sockaddr_in &Socket::getSocketHint() const { return socketHint; }
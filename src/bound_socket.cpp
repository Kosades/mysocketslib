#include "bound_socket.hpp"
#include "exceptions/bound_socket_exceptions.hpp"
#include <cerrno>
#include <unistd.h>

using namespace Sockets;

// TODO create SIGINT handling

BoundSocket::BoundSocket(std::string address, uint16_t port)
    : Socket(address, port) {
  bind();
}
BoundSocket::BoundSocket(int FD, std::string address, uint16_t port)
    : Socket(address, port) {
  boundFD = FD;
}
void BoundSocket::bind() {
  boundFD = socket(AF_INET, SOCK_STREAM, 0);
  if (boundFD == -1) {
    throw parseSocketErrorCode(errno);
  }

  // Binding hint and FD
  int result = ::bind(boundFD, (sockaddr *)&socketHint, sizeof(socketHint));
  if (result == -1) {
    throw parseBindErrorCode(errno);
  }
}

const int BoundSocket::getFileDescriptor() { return boundFD; }

BoundSocket::~BoundSocket() { close(boundFD); }
#include "listen_socket.hpp"
#include "exceptions/listen_socket_exceptions.hpp"
#include <cstring>
#include <netdb.h>
#include <unistd.h>

using namespace Sockets;

ListenSocket::ListenSocket(std::string listenAddress, uint16_t listenPort)
    : BoundSocket(listenAddress, listenPort) {
  isListening = false;
}

void ListenSocket::listen(std::function<void(ConnectSocket)> onConnected) {
  // Starting listen thread
  listenThread = std::thread([&]() { listener(onConnected); });
  // It will be joined in destructor
}
/*Method for accepting a connection if everything is right returns connected
 * socket */
ConnectSocket ListenSocket::accept() {

  // Accepting new connections
  sockaddr_in clientHint;
  socklen_t clientLength = sizeof(clientHint);
  int acceptedFD =
      ::accept(getFileDescriptor(), (sockaddr *)&clientHint, &clientLength);

  // Catching exceptions
  if (acceptedFD == -1)
    throw parseAcceptErrorCode(errno);

  // Creating char arrays to get address and port
  char connectedAddress[NI_MAXSERV];
  char connectedPort[NI_MAXSERV];

  // Putting zero values into those arrays
  memset(connectedAddress, 0, NI_MAXSERV);
  memset(connectedPort, 0, NI_MAXSERV);

  // Getting connected address and port
  int result =
      getnameinfo((sockaddr *)&clientHint, clientLength, connectedAddress,
                  NI_MAXSERV, connectedPort, NI_MAXSERV, 0);

  if (result == 0) {
    inet_ntop(AF_INET, &clientHint.sin_addr, connectedAddress, NI_MAXHOST);
  }

  return ConnectSocket(acceptedFD, getAddress(), htons(clientHint.sin_port),
                       connectedAddress, atoi(connectedPort));
}

void ListenSocket::listener(std::function<void(ConnectSocket)> onConnected) {
  // Putting FD into listening mode
  int result = ::listen(getFileDescriptor(), SOMAXCONN);
  // Catching exception
  if (result == -1)
    throw parseListenErrorCode(errno);
  // Beggining listening
  isListening = true;
  while (isListening) {
    ConnectSocket *acceptedSocket = new ConnectSocket(accept());

    /*starting thread with user function*/
    threads.push_back(std::thread([onConnected, acceptedSocket]() {
      onConnected(*acceptedSocket);
      delete acceptedSocket;
    }));
  }

  // After listening is closed, waiting for completion of other threads
  for (std::_List_iterator<std::thread> iterator = threads.begin();
       iterator != threads.end(); iterator++) {
    (*iterator).join();
  }
  threads.clear();
}

ListenSocket::~ListenSocket() { listenThread.join(); }

#include "connect_socket.hpp"
#include "exceptions/connected_sockets_exceptions.hpp"
#include "listen_socket.hpp"
#include <cstring>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace Sockets;

ConnectSocket::ConnectSocket(std::string address, uint16_t port)
    : BoundSocket(address, port), connectedTo(Socket(address, port)) {
  isConnected = false;
}

ConnectSocket::ConnectSocket(int FD, std::string currentAddress,
                             uint16_t currentPort, std::string connectedAddress,
                             uint16_t connectedPort)
    : BoundSocket(FD, currentAddress, currentPort),
      connectedTo(connectedAddress, connectedPort) {
  isConnected = true;
}

void ConnectSocket::connect(std::string address, int port) {
  connectedTo = Socket(address, port);
  int result =
      ::connect(getFileDescriptor(), (sockaddr *)&connectedTo.getSocketHint(),
                INET_ADDRSTRLEN);
  if (result == -1) {
    throw parseConnectErrorCode(errno);
  }
  isConnected = true;
}
char *ConnectSocket::receive(size_t receiveBytes) {
  char *receiveBuffer = new char[receiveBytes];
  ssize_t receivedBytes =
      recv(getFileDescriptor(), receiveBuffer, receiveBytes, 0);

  if (receivedBytes == -1) {
    throw parseReceiveErrorCode(errno);
  }

  return receiveBuffer;
}
void ConnectSocket::send(char *buffer, size_t bufferLength) {
  int result = ::send(getFileDescriptor(), buffer, bufferLength + 1, 0);
  if (result == -1) {
    throw parseSendErrorCode(errno);
  }
}

const Socket &ConnectSocket::getConnectedSocket() { return connectedTo; }
bool ConnectSocket::isSocketConnected() { return isConnected; }

# How to install

Installation process is very easy(I hope) and it consists of two parts:

1. Building library
2. Building your program with linked library

Also you can build this lib statically with your program(there is example in tests/StaticCompile)

So, you open terminal in library's root folder and type:

> mkdir build ; cd build

Then you generate configurations in CMake(you can use GUI if you want):

> cmake ..

And finally build this lib:

> make

If you need, you can install it by this command:

> sudo make install

After this install you can directly use sockets.hpp in your projects and only need to link this library to it
